<?php

namespace SimpleMath;

/**
 * Class Resolver
 * Takes string with mathematics expression, sanitizes and tries to calculate result.
 * CAUTION: eval() function must be executable.
 * @package SimpleMath
 */
class Resolver {
    private $expression;
    private $allowedCharacters = '/\d|\*|\/|\+|\-|\(|\)/';

    public function __construct(String $expression)
    {
        $this->expression = $this->sanitizeExpression($expression);
    }

    /**
     * Take a numbers, math operators and brackets from $expression
     * @param String $expression
     * @return string
     */
    private function sanitizeExpression(String $expression)
    {
        $clearExpression = '';
        $matches = array();
        preg_match_all($this->allowedCharacters, $expression, $matches);

        if (count($matches[0]) > 0) {
            $clearExpression = implode('', $matches[0]);
        }

        return $clearExpression;
    }

    /**
     * Try to execute sanitized expression by using eval.
     * Here you can use some external service (like Wolfram Alpha).
     * @return float
     * @throws \Exception
     */
    public function calculate()
    {
        try {
            $answer = (float)eval("return {$this->expression};");
        }
        catch (\ParseError $exception) {
            throw new \Exception("Parse error. Check you expression: {$this->expression}");
        }

        return $answer;
    }
}