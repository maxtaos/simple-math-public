<?php

namespace SimpleMath;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SimpleMath extends Command {
    protected function configure()
    {
        $this->setName('SimpleMath')
            -> setDescription('Simple mathematics console app for code demonstration.')
            -> setHelp('Write string with mathematics expression and will see the result. Supports only base mathematics operations and brackets.')
            -> addArgument('expression', InputArgument::REQUIRED, 'String with mathematics expression');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $resolver = new Resolver($input->getArgument('expression'));
            $answer = $resolver->calculate();

            $output->write("Answer: {$answer}\n");
        }
        catch (\Exception $exception) {
            $output->write("{$exception->getMessage()}\n");
        }
    }
}