<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use SimpleMath\SimpleMath;

$app = new Application('Simple Math', 'v1.0.0');
$app->add(new SimpleMath());
$app->run();