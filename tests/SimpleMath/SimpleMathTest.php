<?php

namespace SimpleMath\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use SimpleMath\SimpleMath;
use Symfony\Component\Console\Tester\CommandTester;

class SimpleMathTest extends TestCase
{
    private $commandTester;
    private $commandName;

    public function setUp()
    {
        $application = new Application();
        $application->add(new SimpleMath());

        $command = $application->find('SimpleMath');
        $this->commandName = $command->getName();
        $this->commandTester = new CommandTester($command);
    }

    public function getTestExpressions()
    {
        return [
            /* Test cases with basic operations */
            ['1+2', 'Answer: 3'],
            ['3-2', 'Answer: 1'],
            ['7*9', 'Answer: 63'],
            ['81/9', 'Answer: 9'],

            /* Test case with expression than contents brackets and all available operations*/
            ['(2+3*(6-2))/2', 'Answer: 7'],

            /* Test case with invalid expression */
            ['1+3*(2/', 'Parse error. Check you expression: 1+3*(2/'],

            /* Test cases for sanitizing */
            ['1q+2w-1e 34 y/1_wedfg3@#$%4;', 'Answer: 2'],
            ['exec();1+2-die();', 'Parse error. Check you expression: ()1+2-()'],
        ];
    }

    /**
     * @dataProvider getTestExpressions
     */
    public function testSimpleMath(String $expression, String $answer)
    {
        $this->commandTester->execute([
            'command'  => $this->commandName,
            'expression' => $expression,
        ]);
        $output = $this->commandTester->getDisplay();
        $this->assertContains($answer, $output);
    }
}